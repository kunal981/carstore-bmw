//
//  HomeMenuViewController.m
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "HomeMenuViewController.h"
#import "WebService.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "Constant.h"

@interface HomeMenuViewController ()
{
    WebService * webService_Obj;
    
}

@end

@implementation HomeMenuViewController
@synthesize mapView,coordinate,boundingMapRect;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self homeApi];
    [self getSettingInfo];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [self.imgViewBanner setUserInteractionEnabled:YES];
    [self.imgViewBanner addGestureRecognizer:singleTap];
    
    
    
   /*NSString *str = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?address=1740 W 5th Ave.Vancouver B.C. V6J 1P2&region=can"];
    NSLog(@"%@",str);
    
    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
   NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    
    
    
    NSLog(@"About to send req %@",req.URL);
    NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    if (data==nil) {
        //UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Profile not updated" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //[alert show];
    }else {
        id result = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSLog(@"%@",result);
    
    NSArray *locationArray =[[[result valueForKey:@"results"] valueForKey:@"geometry"] valueForKey:@"location"];
        
   
        NSLog(@"%@",locationArray);
        
        NSString *lat = [[locationArray objectAtIndex:0] valueForKey:@"lat"];
        NSString *lng = [[locationArray objectAtIndex:0]valueForKey:@"lng"];
        
        NSLog(@"%@,%@",lat,lng);
        
        destinationLatitude = [[NSString stringWithFormat:@"%@",lat] floatValue];
        destinationLongitude = [[NSString stringWithFormat:@"%@",lng] floatValue];
        [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithFloat:destinationLatitude] forKey:@"destLat"];
        [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithFloat:destinationLongitude] forKey:@"destLng"];
        [[NSUserDefaults standardUserDefaults]synchronize];

        NSLog(@"%f,%f",destinationLatitude,destinationLongitude);*/

    //}
   
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.numberOfLines = 2;
    label.font = [UIFont boldSystemFontOfSize: 16.0f];
    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [label setTextAlignment:NSTextAlignmentCenter];
    label.textColor = [UIColor whiteColor];
    label.text = navTitle;
    self.navigationItem.titleView = label;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    
    self.btnRoad.backgroundColor =  [UIColor colorWithRed:0.2627 green:0.5804 blue:0.9647 alpha:1.0];
   
    
    self.btnRoad.layer.cornerRadius = 3.0;
    self.btnRoad.layer.masksToBounds  = YES;
    
    self.btnCall.layer.cornerRadius = 3.0;
    self.btnCall.layer.masksToBounds  = YES;
    
    self.btnEmail.layer.cornerRadius = 3.0;
    self.btnEmail.layer.masksToBounds  = YES;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStyleDone target:self action:@selector(menuPressed)];
    
    
    
    
   // [self mapDirection];
      
    // Do any additional setup after loading the view from its nib.
}

- (void)showProgressForView:(UIView *)view WithMessage:(NSString *)message {
    self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    _hud.labelText = message;
}

- (void)hideProgressForView:(UIView*)view {
    if (_hud) {
        [MBProgressHUD hideHUDForView:view animated:YES];
        self.hud = nil;
    }
}

#pragma mark Home_Page API
-(void)homeApi
{
    [self showProgressForView:self.navigationController.view WithMessage:@"Loading..."];
    
    webService_Obj=[WebService shared];
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    [dict setValue:@"show_home_data" forKey:@"action"];
     [dict setValue:company_id forKey:@"company_app_id"];
    
     [webService_Obj POST:dict completion:^(NSDictionary *json, BOOL sucess) {
         
         [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
         NSLog(@"%@",json);
         
         NSArray *comapny_info1 = [json valueForKey:@"company_info"];
         
         NSLog(@"%@",[[comapny_info1 objectAtIndex:0]valueForKey:@"homepage_banner"]);
         
         
         [self.imgViewBanner sd_setImageWithURL:[NSURL URLWithString:[[comapny_info1 objectAtIndex:0]valueForKey:@"homepage_banner"]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
         
         NSString *strBannerNumber = [[comapny_info1 objectAtIndex:0] valueForKey:@"_banner_no"];
         NSString *strCallNumber = [[comapny_info1 objectAtIndex:0]valueForKey:@"company_call"];
         NSString *roadSideNumber = [[comapny_info1 objectAtIndex:0]valueForKey:@"_roadside_assitance"];
         NSString *strEmail = [[comapny_info1 objectAtIndex:0]valueForKey:@"email"];
         NSString *lat =[[comapny_info1 objectAtIndex:0]valueForKey:@"latitude"];
         NSString *lng = [[comapny_info1 objectAtIndex:0]valueForKey:@"longitude"];
         
         [[NSUserDefaults standardUserDefaults]setValue:[[comapny_info1 objectAtIndex:0]valueForKey:@"_special"] forKey:@"SpecialLink"];
         [[NSUserDefaults standardUserDefaults]synchronize];
         
         bannerNumber = [NSString stringWithFormat:@"%@",strBannerNumber];
         btnCallNumber = [NSString stringWithFormat:@"%@",strCallNumber];
         btnRoadSideNumber =[NSString stringWithFormat:@"%@",roadSideNumber];
         btn_Email =[NSString stringWithFormat:@"%@",strEmail];
         
         destinationLatitude = [[NSString stringWithFormat:@"%@",lat] floatValue];
         destinationLongitude = [[NSString stringWithFormat:@"%@",lng] floatValue];
         [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithFloat:destinationLatitude] forKey:@"destLat"];
         [[NSUserDefaults standardUserDefaults]setValue:[NSNumber numberWithFloat:destinationLongitude] forKey:@"destLng"];
         [[NSUserDefaults standardUserDefaults]synchronize];
         [self viewWillAppear:YES];
         
         
     }];
}

-(void)getSettingInfo{
    //[self showProgressForView:self.navigationController.view WithMessage:@"Loading..."];
    webService_Obj=[WebService shared];
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
    NSString * uid=[[[UIDevice currentDevice]identifierForVendor] UUIDString];
    NSLog(@"uuid = %@",uid);
    
    [dict setValue:@"show_user_info" forKey:@"action"];
    [dict setValue:company_id forKey:@"company_app_id"];
    [dict setValue:uid forKey:@"device_token"];
    NSLog(@"%@",dict);
    
    [webService_Obj POST:dict completion:^(NSDictionary *json, BOOL sucess) {
       // [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
        NSLog(@"%@",json);
        comapny_info = [json valueForKey:@"user_info"];
        
        if (comapny_info == nil || comapny_info ==(id)[NSNull null] || comapny_info.count==0) {
            
         
            //[self performSelectorOnMainThread:@selector(DataAlert) withObject:nil waitUntilDone:NO];
            
        }
        else {
            
            first_name= [[comapny_info objectAtIndex:0]valueForKey:@"first_name"];
            last_name = [[comapny_info objectAtIndex:0]valueForKey:@"last_name"];
           // btnEmail = [[comapny_info objectAtIndex:0]valueForKey:@"email_address"];
            mobile_no = [[comapny_info objectAtIndex:0]valueForKey:@"phone_mobile"];
            home_no = [[comapny_info objectAtIndex:0]valueForKey:@"phone_home"];
            busniess_no = [[comapny_info objectAtIndex:0]valueForKey:@"phone_bussiness"];
            
            vin_no = [[comapny_info objectAtIndex:0]valueForKey:@"vehicle_info"];
            
            service_option = [[comapny_info objectAtIndex:0]valueForKey:@"service_option"];
            
            repair_option =  [[comapny_info objectAtIndex:0]valueForKey:@"repair"];
            
            email_body =[NSString stringWithFormat:@"VIN:%@\nCustomer Name:%@ %@\nCustomer phone number(Home):%@\nCustomer phone number(Mobile):%@\nCustomer phone number(Business):%@\nDrop off required:%@\nRepair completion text required:%@",vin_no,first_name,last_name,home_no,mobile_no,busniess_no,service_option,repair_option];
            NSLog(@"%@",email_body);
            
            
        }
            
    }];
    
}

-(void)DataAlert{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter your information in setting page" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    sourceLatitude = locationManager.location.coordinate.latitude;
    sourceLongitude = locationManager.location.coordinate.longitude;
    NSLog(@"%f",sourceLatitude);
    NSLog(@"%f",sourceLongitude);
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation*)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    CLLocationCoordinate2D newcordinate =   newLocation.coordinate;
    CLLocationCoordinate2D oldcordinate =   oldLocation.coordinate;
    
    MKMapPoint * pointsArray =
    malloc(sizeof(CLLocationCoordinate2D)*2);
    
    pointsArray[0]= MKMapPointForCoordinate(oldcordinate);
    pointsArray[1]= MKMapPointForCoordinate(newcordinate);
    
    MKPolyline *  routeLine = [MKPolyline polylineWithPoints:pointsArray count:2];
    free(pointsArray);
    
    [mapView addOverlay:routeLine];  //MkMapView declared in .h
}

//MKMapViewDelegate

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id)overlay {
    
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolylineView* aView = [[MKPolylineView alloc]initWithPolyline:(MKPolyline*)overlay] ;
        aView.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.5];
        aView.lineWidth = 10;
        return aView;
    }
    return nil;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
   
    CLLocationCoordinate2D location = mapView.userLocation.coordinate;
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    
    location.latitude  = destinationLatitude;
    location.longitude = destinationLongitude;
    
   
    
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;
    
    region.span = span;
    region.center = location;
    
    [mapView setRegion:region animated:YES];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake(destinationLatitude, destinationLongitude);
    
    annotation.title = mapAddress;
    [mapView addAnnotation:annotation];

    
}

-(void)menuPressed{
    CGRect destination = self.navigationController.view.frame;
    
    if (destination.origin.x > 0)
    {
        destination.origin.x = 0;
    }else
    {
        destination.origin.x = 180;
    }
    
    [UIView animateWithDuration:0.25 animations:^
     {
         self.navigationController.view.frame = destination;
         
     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)segmentedBtn:(id)sender {
    
    if(self.segmentedBtn.selectedSegmentIndex==0){
        
        
    }
    else {
        
    }
}

#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
  //  [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)btnRoadSide:(id)sender {
    
    //NSString *phNo = @"+18002678269";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",btnRoadSideNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView  *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}



#pragma mark - Utility Methods
- (void)plotRouteOnMap:(MKRoute *)route
{
    
    
    // Update the ivar
    _routeOverlay = route.polyline;
    
    // Add it to the map
    [self.mapView addOverlay:_routeOverlay];
    
}


#pragma mark - MKMapViewDelegate methods

-(void)mapDirection{
    
    float lat  =49.268858;
    float longitude =-123.140829;
    MKPlacemark *source = [[MKPlacemark   alloc]initWithCoordinate:CLLocationCoordinate2DMake(lat,longitude)   addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    MKMapItem *srcMapItem = [[MKMapItem alloc]initWithPlacemark:source];
    [srcMapItem setName:@""];
    // float lat1  =49.2671202;
    // float longitude2 =-123.1433779;
    MKPlacemark *destination = [[MKPlacemark alloc]initWithCoordinate:CLLocationCoordinate2DMake(destinationLatitude, destinationLongitude) addressDictionary:[NSDictionary dictionaryWithObjectsAndKeys:@"",@"", nil] ];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake(sourceLatitude, sourceLongitude);
    
    annotation.title = @"Current";
    [mapView addAnnotation:annotation];
    
    
    MKMapItem *distMapItem = [[MKMapItem alloc]initWithPlacemark:destination];
    [distMapItem setName:@""];
    
    MKDirectionsRequest *request = [[MKDirectionsRequest alloc]init];
    [request setSource:srcMapItem];
    [request setDestination:distMapItem];
    [request setTransportType:MKDirectionsTransportTypeWalking];
    
    MKDirections *direction = [[MKDirections alloc]initWithRequest:request];
    
    [direction calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        
        NSLog(@"response = %@",response);
        NSArray *arrRoutes = [response routes];
        [arrRoutes enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            MKRoute *rout = obj;
            
            MKPolyline *line = [rout polyline];
            [mapView addOverlay:line];
            NSLog(@"Rout Name : %@",rout.name);
            NSLog(@"Total Distance (in Meters) :%f",rout.distance);
            
            NSArray *steps = [rout steps];
            
            NSLog(@"Total Steps : %lu",(unsigned long)[steps count]);
            
            [steps enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSLog(@"Rout Instruction : %@",[obj instructions]);
                NSLog(@"Rout Distance : %f",[obj distance]);
                
                
                MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
                annotation.coordinate = CLLocationCoordinate2DMake(destinationLatitude, destinationLongitude);
                
                annotation.title = @"BMW Store";
                [mapView addAnnotation:annotation];
                
               
            }];
        }];
    }];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
    renderer.strokeColor = [UIColor redColor];
    renderer.lineWidth = 4.0;
    return  renderer;
}

#pragma mark -UITapGesture Method

-(void)tapDetected{
    
   // NSString *phNo = @"+6046593250";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",bannerNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView  *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [calert show];
        NSLog(@"dgggghghhjy");
    }
}
#pragma mark -UIButton Action


- (IBAction)btnEmail:(id)sender {
    
    if (comapny_info.count==0) {
        [self performSelectorOnMainThread:@selector(DataAlert) withObject:nil waitUntilDone:NO];
    }else {
    
    mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    [mailComposer setSubject:@"Automated Repair Request"];
    NSArray *toBcc = [NSArray arrayWithObject:@"dealernx@gmail.com"];
    [mailComposer setBccRecipients:toBcc];

    
    
     NSArray *toRecipients = [NSArray arrayWithObject:btn_Email];
    [mailComposer setToRecipients:toRecipients];
  
    [mailComposer setMessageBody:email_body isHTML:NO];
    [self presentViewController:mailComposer animated:YES completion:nil];
    }
}

- (IBAction)btnCall:(id)sender {
    
   // NSString *phNo = @"+6046593250";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",btnCallNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView  *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}
@end
