//
//  RequestServiceViewController.h
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
@class MBProgressHUD;

@interface RequestServiceViewController : UIViewController<MFMailComposeViewControllerDelegate>{
    MFMailComposeViewController *mailComposer;
    
    NSString *btnCallNumber;
    NSString *btnRoadSideNumber;
    NSString *btnEmail;
    NSString *strContent;
    
    NSString *first_name;
    NSString *last_name;
    NSString *vin_no;
    NSString *service_option;
    NSString *repair_option;
    NSString *home_no;
    NSString *mobile_no;
    NSString *busniess_no;
    NSString *email_body;
    NSArray *comapny_info;
    
}
@property (strong, nonatomic) IBOutlet UILabel *lblRoadSide;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBanner;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
- (IBAction)btnCall:(id)sender;
- (IBAction)btnEmail:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblContent;

@property (strong, nonatomic) IBOutlet UIButton *btnRoadSide;
- (IBAction)btnRoadSide:(id)sender;
@property (strong, nonatomic) MBProgressHUD *hud;
@end
