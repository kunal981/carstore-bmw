//
//  AppDelegate.h
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@class HomeMenuViewController;
@class SettingViewController;
@class RequestServiceViewController;
@class ClusterViewController;
@class SpecialViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate,UIScrollViewDelegate>{
    HomeMenuViewController *homeVC;
    SettingViewController *settingVC;
    RequestServiceViewController *requestVC;
    ClusterViewController *clusterVC;
    SpecialViewController * specialVC;
    UILabel* installerLabel;
    UIView* view;
    UIScrollView* lableScroll;
    UIButton* btn1,*btn2,*btn3,*btn4,*btn5,*btn6,*btn7;
    
     NSMutableArray* arrayForImages;
    
    UIImageView* btni;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;


@end

