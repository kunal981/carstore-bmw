//
//  MapViewController.h
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>{
    
    CLLocation *currentLocation;
    
    MKPinAnnotationView * pinView;
    
   
    
    NSMutableArray *finalArray;
    
   
    
   
    
}
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic, strong)MKPointAnnotation *myAnnotation ;

@property (nonatomic, strong) CLGeocoder *myGeocoder;



@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) NSMutableDictionary *routeDataDict;



- (NSMutableArray *)decodePolyLine: (NSMutableString *)encoded;


@end
