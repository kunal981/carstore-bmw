//
//  Constant.h
//  CarDealer
//
//  Created by brst on 27/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject
    
extern NSString* const company_id;
extern NSString *const mapAddress;
extern NSString* const navTitle;
extern float  geoLatitude;
extern float  geoLongitude;
extern int geoRadius;

@end
