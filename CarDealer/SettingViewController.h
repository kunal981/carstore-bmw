//
//  SettingViewController.h
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
#import <CoreLocation/CoreLocation.h>
@class MBProgressHUD;
@interface SettingViewController : UIViewController<ZBarReaderDelegate,CLLocationManagerDelegate,UITextFieldDelegate,UITextFieldDelegate>{
    CLLocationManager *locationManger;
    float latitude;
    float longitude;
    NSString *geoFenceStatus;
    NSString *serviceStatus;
    NSString *repairStatus;
}
@property (strong, nonatomic) MBProgressHUD *hud;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UIButton *btnSave;
- (IBAction)btnScan:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldVIN;

- (IBAction)btnServiceOption1:(id)sender;
- (IBAction)btnServiceOption2:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtFirstName;
@property (strong, nonatomic) IBOutlet UITextField *txtLastName;

- (IBAction)btnSave:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (strong, nonatomic) IBOutlet UITextField *txtPhoneNo;
- (IBAction)switchGeoFence:(id)sender;
- (IBAction)switchServiceOption1:(id)sender;
- (IBAction)switchServiceoption2:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txtMobile;
@property (strong, nonatomic) IBOutlet UITextField *txtBusiness;
@property (strong, nonatomic) IBOutlet UISwitch *switchServiceOption1;
@property (strong, nonatomic) IBOutlet UISwitch *switchServiceOption2;
@property (strong, nonatomic) IBOutlet UISwitch *switchGeoFence;


@end
