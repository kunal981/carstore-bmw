//
//  ClusterViewController.h
//  CarDealer
//
//  Created by brst on 09/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
@class MBProgressHUD;
@interface ClusterViewController : UIViewController<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate>{
     UICollectionView *_collectionView;
    MFMailComposeViewController *mailComposer;
    NSString *strWebView;
    NSString *phNo;
    NSString *btnEmail;
    
    NSString *first_name;
    NSString *last_name;
    NSString *vin_no;
    NSString *service_option;
    NSString *repair_option;
    NSString *home_no;
    NSString *mobile_no;
    NSString *busniess_no;
    NSString *email_body;
    NSArray *comapny_info;

    
}



@property (strong, nonatomic) MBProgressHUD *hud;
@property (nonatomic, strong) IBOutlet UIView *alertView;
- (IBAction)btnHidePressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;

@property (strong, nonatomic) IBOutlet UILabel *LBLURSA;
@property (strong, nonatomic) IBOutlet UILabel *lblWas;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewPopUp;
@property (strong, nonatomic) IBOutlet UIView *viewPopUp;
- (IBAction)btnCall:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
- (IBAction)btnEmail:(id)sender;

@end
