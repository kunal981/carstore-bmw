//
//  AppDelegate.m
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeMenuViewController.h"
#import "MapViewController.h"
#import "SettingViewController.h"
#import "RequestServiceViewController.h"
#import "SpecialViewController.h"
#import "ClusterViewController.h"
#import "GasMileageFormViewController.h"

//#import "CustomAnimationController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    if ([UIScreen mainScreen].bounds.size.height==736) {
       homeVC = [[HomeMenuViewController alloc]initWithNibName:@"HomeMenuViewController~6+" bundle:nil];
    }
    
    else if ([UIScreen mainScreen].bounds.size.height==480){
        homeVC = [[HomeMenuViewController alloc]initWithNibName:@"HomeMenuViewController~4s" bundle:nil];
    }
    
    
    else {
        homeVC = [[HomeMenuViewController alloc]init];

    }
  
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    
    self.navController =[[UINavigationController alloc]initWithRootViewController:homeVC];
    
    UIImage *nav_backgroundImg = [UIImage imageNamed:@"navBar"];
    
    [[UINavigationBar appearance] setBackgroundImage:nav_backgroundImg forBarMetrics:UIBarMetricsDefault];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.window.rootViewController = self.navController;
    UIColor * myColor=[UIColor whiteColor];
    UIColor * btn_Background_Color=[UIColor clearColor];
    
    view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 260, 568)];
    view.backgroundColor=[UIColor blackColor];
    
    UIView  * menuView=[[UIView alloc]initWithFrame:CGRectMake(0,20, view.frame.size.width, 44)];
    menuView.backgroundColor=[UIColor colorWithRed:0.1961 green:0.4314 blue:0.8039 alpha:1.0];
    [view addSubview:menuView];
    
    UILabel * lbl_menu=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, view.frame.size.width-20, 30)];
   // lbl_menu.text=@"Menu";
    lbl_menu.textColor=[UIColor whiteColor];
    lbl_menu.textAlignment=NSTextAlignmentLeft;
    [menuView addSubview:lbl_menu];
    
    lableScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 68, 260,600)];
    lableScroll.backgroundColor = [UIColor redColor];
    
    lableScroll.delegate = self;
    lableScroll.showsVerticalScrollIndicator = NO;
    lableScroll.showsHorizontalScrollIndicator = NO;
    lableScroll.backgroundColor=[UIColor clearColor];
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 568)
        {
            lableScroll.contentSize=CGSizeMake(200, 300);
        }
        else
        {
            lableScroll.contentSize=CGSizeMake(200, 424);
        }
    }
    else
    {
        lableScroll.contentSize=CGSizeMake(200, 100);
        
    }
    [view addSubview:lableScroll];
    
    
    installerLabel=[[UILabel alloc]initWithFrame:CGRectMake(75, 165, 180, 35)];
    installerLabel.font=[UIFont boldSystemFontOfSize:17];
    installerLabel.textColor=[UIColor whiteColor];
    
    
    
   
    
        int x=10;
    int width=220;
    int height=1;
    
    btn1=[UIButton buttonWithType:UIButtonTypeSystem];
    btn1.frame=CGRectMake(x, 0, width, 32);
    [btn1 setTitle:@"Home" forState:UIControlStateNormal];
    btn1.titleLabel.textAlignment=NSTextAlignmentLeft;
   // btn1.titleEdgeInsets=UIEdgeInsetsMake(0, -160, 0, 0);
    btn1.tag=1;
    btn1.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    btn1.backgroundColor=btn_Background_Color;
   btn1.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [btn1 setTitleColor:myColor forState:UIControlStateNormal];
    [btn1 addTarget:self action:@selector(taskWork:) forControlEvents:UIControlEventTouchUpInside];
    [lableScroll addSubview:btn1];
   
    
    int y=btn1.frame.origin.y+btn1.frame.size.height;
    for (int i=0; i<6; i++)
    {
        UIView * line_View=[[UIView alloc]initWithFrame:CGRectMake(0, y, view.frame.size.width, 1)];
        line_View.backgroundColor=[UIColor colorWithRed:0.1961 green:0.4314 blue:0.8039 alpha:0.7];
        y+=btn1.frame.origin.y+btn1.frame.size.height+1;
        [lableScroll addSubview:line_View];
    }

    btn2=[UIButton buttonWithType:UIButtonTypeSystem];
    btn2.frame=CGRectMake(x, btn1.frame.size.height + btn1.frame.origin.y + height, width, 32);
    [btn2 setTitle:@"Find Us" forState:UIControlStateNormal];
    btn2.tag=2;
     btn2.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    btn2.titleLabel.textAlignment=NSTextAlignmentLeft;
    btn2.backgroundColor=btn_Background_Color;
    btn2.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [btn2 addTarget:self action:@selector(taskWork:) forControlEvents:UIControlEventTouchUpInside];
    [btn2 setTitleColor:myColor forState:UIControlStateNormal];
    [lableScroll addSubview:btn2];
    
    btn3=[UIButton buttonWithType:UIButtonTypeSystem];
    btn3.frame=CGRectMake(x, btn2.frame.size.height + btn2.frame.origin.y + height, width, 32);
    [btn3 setTitle:@"Request Service" forState:UIControlStateNormal];
    btn3.tag=3;
    btn3.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    btn3.titleLabel.textAlignment=NSTextAlignmentLeft;
    btn3.backgroundColor=btn_Background_Color;
    btn3.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [btn3 addTarget:self action:@selector(taskWork:) forControlEvents:UIControlEventTouchUpInside];
    [btn3 setTitleColor:myColor forState:UIControlStateNormal];
    [lableScroll addSubview:btn3];
    
    btn4=[UIButton buttonWithType:UIButtonTypeSystem];
    btn4.frame=CGRectMake(x, btn3.frame.size.height + btn3.frame.origin.y + height, width, 32);
    [btn4 setTitle:@"Instrument Cluster" forState:UIControlStateNormal];
    btn4.tag=4;
    btn4.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    btn4.backgroundColor=btn_Background_Color;
    btn4.titleLabel.textAlignment=NSTextAlignmentLeft;
    btn4.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [btn4 addTarget:self action:@selector(taskWork:) forControlEvents:UIControlEventTouchUpInside];
    [btn4 setTitleColor:myColor forState:UIControlStateNormal];
    [lableScroll addSubview:btn4];
    
    btn5=[UIButton buttonWithType:UIButtonTypeSystem];
    btn5.frame=CGRectMake(x, btn4.frame.size.height + btn4.frame.origin.y + height, width, 32);
    [btn5 setTitle:@"Specials" forState:UIControlStateNormal];
    btn5.tag=6;
    btn5.backgroundColor=btn_Background_Color;
    btn5.titleLabel.textAlignment=NSTextAlignmentLeft;
    btn5.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    btn5.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [btn5 addTarget:self action:@selector(taskWork:) forControlEvents:UIControlEventTouchUpInside];
    [btn5 setTitleColor:myColor forState:UIControlStateNormal];
    [lableScroll addSubview:btn5];
    
    btn6=[UIButton buttonWithType:UIButtonTypeSystem];
    btn6.frame=CGRectMake(x, btn5.frame.size.height + btn5.frame.origin.y + height, width, 32);
    [btn6 setTitle:@"Settings" forState:UIControlStateNormal];
    btn6.tag=5;
    btn6.backgroundColor=btn_Background_Color;
    btn6.titleLabel.textAlignment=NSTextAlignmentLeft;
    btn6.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    btn6.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [btn6 addTarget:self action:@selector(taskWork:) forControlEvents:UIControlEventTouchUpInside];
    [btn6 setTitleColor:myColor forState:UIControlStateNormal];
    [lableScroll addSubview:btn6];
    
    btn7=[UIButton buttonWithType:UIButtonTypeSystem];
    btn7.frame=CGRectMake(x, btn5.frame.size.height + btn6.frame.origin.y + height, width, 32);
    [btn7 setTitle:@"Gas Tracker" forState:UIControlStateNormal];
    btn7.tag=7;
    btn7.backgroundColor=btn_Background_Color;
    btn7.titleLabel.textAlignment=NSTextAlignmentLeft;
    btn7.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    btn7.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [btn7 addTarget:self action:@selector(taskWork:) forControlEvents:UIControlEventTouchUpInside];
    [btn7 setTitleColor:myColor forState:UIControlStateNormal];
   // [lableScroll addSubview:btn7];

    
    UIImageView * logo_Img;
    if (!IS_IPHONE_5)
    {
         logo_Img=[[UIImageView alloc]initWithFrame:CGRectMake(40, btn5.frame.size.height + btn5.frame.origin.y + 130,100, 100)];
    }else
    {
         logo_Img=[[UIImageView alloc]initWithFrame:CGRectMake(25, btn5.frame.size.height + btn5.frame.origin.y + 210,120, 120)];
    }
    
    logo_Img.layer.masksToBounds=YES;
    logo_Img.image=[UIImage imageNamed:@"bmw.png"];
    logo_Img.backgroundColor=[UIColor clearColor];
    [lableScroll addSubview:logo_Img];
    
//    btn6=[UIButton buttonWithType:UIButtonTypeSystem];
//    btn6.frame=CGRectMake(55, 274,90, 32);
//    [btn6 setTitle:@"STATISTICS" forState:UIControlStateNormal];
//    btn6.tag=6;
//    btn6.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
//    [btn6 addTarget:self action:@selector(taskWork:) forControlEvents:UIControlEventTouchUpInside];
//    [btn6 setTitleColor:[UIColor colorWithRed:1.0000 green:0.9490 blue:0.8863 alpha:8.0] forState:UIControlStateNormal];
//    [lableScroll addSubview:btn6];
    
    [self.window addSubview:view];
    [self.window makeKeyAndVisible];
    
    // Override point for customization after application launch.
    return YES;
}


-(void)taskWork:(UIButton *)sender
{
    if (sender.tag==1)
    {
        
        if ([UIScreen mainScreen].bounds.size.height==736) {
            homeVC = [[HomeMenuViewController alloc]initWithNibName:@"HomeMenuViewController~6+" bundle:nil];
        }
        else if ([UIScreen mainScreen].bounds.size.height==480){
           homeVC = [[HomeMenuViewController alloc]initWithNibName:@"HomeMenuViewController~4s" bundle:nil];
        }
        else {
            homeVC = [[HomeMenuViewController alloc]init];
            
        }
        UINavigationController* navForNaviChat=[[UINavigationController alloc]initWithRootViewController:homeVC];
        
        self.window.rootViewController=navForNaviChat;
        

        
        
    }
    else if (sender.tag==2)
    {
        MapViewController* mapVC=[[MapViewController alloc]init];
        UINavigationController* navForNaviChat=[[UINavigationController alloc]initWithRootViewController:mapVC];
         mapVC.title=@"Find Us";
        self.window.rootViewController=navForNaviChat;

        
    }
    
else if (sender.tag==3)
{
    if ([UIScreen mainScreen].bounds.size.height==736) {
       requestVC=[[RequestServiceViewController alloc]initWithNibName:@"RequestServiceViewController~6+" bundle:nil];
    }
    
    else if ([UIScreen mainScreen].bounds.size.height==480){
        requestVC=[[RequestServiceViewController alloc]initWithNibName:@"RequestServiceViewController~4s" bundle:nil];

    }
    else {
      requestVC=[[RequestServiceViewController alloc]init];
    }
    
    
                UINavigationController* navForNaviChat=[[UINavigationController alloc]initWithRootViewController:requestVC];
                requestVC.title=@"Request Service";
                self.window.rootViewController=navForNaviChat;

}
    else if (sender.tag==4)
    {
        
        if ([UIScreen mainScreen].bounds.size.height==736) {
             clusterVC=[[ClusterViewController alloc]initWithNibName:@"ClusterViewController~6+" bundle:nil];
        }
         else if ([UIScreen mainScreen].bounds.size.height==480){
              clusterVC=[[ClusterViewController alloc]initWithNibName:@"ClusterViewController~4s" bundle:nil];
         }
        else {
           clusterVC=[[ClusterViewController alloc]init];
        }
        
        
        UINavigationController* navForNaviChat=[[UINavigationController alloc]initWithRootViewController:clusterVC];
         clusterVC.title=@"Instrument Cluster";
        self.window.rootViewController=navForNaviChat;
    }
    
    else if (sender.tag==5)
    {
        if ([UIScreen mainScreen].bounds.size.height==736) {
             settingVC=[[SettingViewController alloc]initWithNibName:@"SettingViewController~6+" bundle:nil];
        }
        else if ([UIScreen mainScreen].bounds.size.height==480){
           settingVC=[[SettingViewController alloc]initWithNibName:@"SettingViewController~4s" bundle:nil];
        }
        else {
             settingVC=[[SettingViewController alloc]init];
            
        }
      
        UINavigationController* navForNaviChat=[[UINavigationController alloc]initWithRootViewController:settingVC];
        settingVC.title=@"Settings";
        self.window.rootViewController=navForNaviChat;
 
        
    }
    else if (sender.tag==6)
    {
        
        specialVC=[[SpecialViewController alloc]init];
        UINavigationController* navForNaviSpecial=[[UINavigationController alloc]initWithRootViewController:specialVC];
        specialVC.title=@"Specials";
        self.window.rootViewController=navForNaviSpecial;
    }
    
    else if (sender.tag==7)
    {
        
        GasMileageFormViewController *gasVC=[[GasMileageFormViewController alloc]init];
        UINavigationController* navForNaviSpecial=[[UINavigationController alloc]initWithRootViewController:gasVC];
        gasVC.title=@"Gas Traker";
        self.window.rootViewController=navForNaviSpecial;
    }
    
    
    else
    {
        NSLog(@"last one ");
        
        
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
