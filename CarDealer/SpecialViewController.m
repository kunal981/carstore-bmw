//
//  SpecialViewController.m
//  CarDealer
//
//  Created by brst on 20/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "SpecialViewController.h"

@interface SpecialViewController ()

@end

@implementation SpecialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor blackColor];
    
    self.navigationController.navigationBar.translucent=NO;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStyleDone target:self action:@selector(menuPressed)];

    
    NSString *fileName= [[NSUserDefaults standardUserDefaults]valueForKey:@"SpecialLink"];
    
     fileName = [fileName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:fileName];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView setScalesPageToFit:YES];
    
    [self.webView loadRequest:request];
    // Do any additional setup after loading the view from its nib.
}
-(void)menuPressed{
    CGRect destination = self.navigationController.view.frame;
    
    if (destination.origin.x > 0)
    {
        destination.origin.x = 0;
    }else
    {
        destination.origin.x = 220;
    }
    
    [UIView animateWithDuration:0.25 animations:^
     {
         self.navigationController.view.frame = destination;
         
     }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
