//
//  ClusterViewController.m
//  CarDealer
//
//  Created by brst on 09/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "ClusterViewController.h"
#import "MBProgressHUD.h"
#import "WebService.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"
#import "KOPopupView.h"

@interface ClusterViewController (){
    WebService * webService_Obj;
    UIImageView *imgView;
    
    NSArray *imgArray;
    NSArray *lbl1Array;
    NSArray *lbl2Array;

}
@property (nonatomic, strong) KOPopupView *popup;

@end

@implementation ClusterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self instrumentClusterApi];
    [self getSettingInfo];
    self.viewPopUp.layer.cornerRadius = 7.0;
    self.viewPopUp.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewPopUp.layer.borderWidth = 1.0;
    self.viewPopUp.backgroundColor =[UIColor colorWithRed:0.2627 green:0.5804 blue:0.9647 alpha:1.0];
    
    
    self.btnClose.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnClose.layer.cornerRadius =  7.0;
    
    self.btnCall.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnCall.layer.cornerRadius =  7.0;
    
    
    self.btnEmail.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnEmail.layer.cornerRadius =  7.0;
    

    NSLog(@"dsd");
    
    imgArray = [[NSArray alloc]initWithObjects:[UIImage imageNamed:@"1.png"],[UIImage imageNamed:@"2.png"],[UIImage imageNamed:@"3.png"],[UIImage imageNamed:@"4.png"],[UIImage imageNamed:@"5.png"],[UIImage imageNamed:@"8.png"],[UIImage imageNamed:@"7.png"],[UIImage imageNamed:@"20.png"],[UIImage imageNamed:@"9.png"],[UIImage imageNamed:@"14.png"],[UIImage imageNamed:@"21.png"],[UIImage imageNamed:@"10.png"],[UIImage imageNamed:@"11.png"],[UIImage imageNamed:@"23.png"],[UIImage imageNamed:@"24.png"],[UIImage imageNamed:@"19.png"],[UIImage imageNamed:@"18.png"],[UIImage imageNamed:@"17.png"],[UIImage imageNamed:@"16.png"],[UIImage imageNamed:@"15.png"],[UIImage imageNamed:@"first_25.png"],[UIImage imageNamed:@"13.png"],[UIImage imageNamed:@"12.png"],[UIImage imageNamed:@"7.png"],[UIImage imageNamed:@"6.png"], nil];
    
    lbl1Array = [[NSArray alloc]initWithObjects:@"The oil pressure is too low",@"The seat belt is unfastened",@"The trunk lid is open",@"Power steering failure",@"Airbag system malfunction",@"The charging current is too low",@"The handbrake is engaged or: The brake fluid level is low",@"The transmission switches to limp home mode",@"The rear Fog light is on",@"Engine-related malfunction",@"Exterior light failure",@"The front hood is open",@"Faulty ABS During heavy braking the brakes may lock up.",@"The washer fluid level is too low",@"The Fuel level is too low",@"Airbag system malfunction",@"If the warning light remains illuminated The coolant temperature is too high If the warning light flashes:High coolant temperature",@"Master warning light The display shows a warning message",@"Diesel engine preheating in progress",@"Worn brake pads",@"High transmission fluid temperature",@"If the warning light flashes:Faulty tyre pressure monitoring (TPMS) system If the warning light remains illuminated Low tyre pressure",@"The service indicator shows the remaining km/time until the next service",@"The brake fluid level is low or:Brake system fault",@"The brake pedal should be pressed", nil];
    
    lbl2Array = [[NSArray alloc]initWithObjects:@"Check the oil level Top up if necessary.Contact our specialist workshop without delay",@"Fasten the seat belt",@"Close the trunk lid",@"Contact our specialist workshop without delay",@"Contact our specialist workshop without delay",@"Contact our specialist workshop without delay",@"Release the handbrake Refill the brake fluid.Contact our specialist workshop without delay",@"Contact our specialist workshop without delay",@" ",@"Turn the ignition off and on.Contact our specialist workshop without delay",@"Check all the exterior lights Renew the faulty lights.Contact our specialist workshop without delay",@"Close the front hood",@"Turn the ignition off and on.Contact our specialist workshop without delay.",@"Top up the washer fluid",@"Refuel the vehicle",@"Contact our specialist workshop without delay",@"If the warning light remains illuminated Stop the vehicle. Turn the engine off. Allow engine to cool down.If the warning light flashes Stop the vehicle Wait for fluid to cool down",@"Contact our specialist workshop without delay.",@"Wait until the warning light goes out Start the engine immediately",@"Contact our specialist workshop without delay",@"Drive carefully at a reduced speed and Contact our specialist workshop without delay",@"If the warning light flashes:Contact our specialist workshop without Delay If the warning light remains illuminated Check tire pressures, adjust if necessary",@"Contact our specialist workshop without delay",@"Check the brake fluid reservoir level Top up if necessary.",@"Press the brake pedal", nil];
    
    
//    popUpView = [[UIView alloc]init];
//    popUpView.frame = CGRectMake(20, 40, self.view.frame.size.width-20, self.view.frame.size.height-40);
//    [self.view addSubview:popUpView];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:_collectionView];
    
    // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg1.png"]];
  
    self.navigationController.navigationBar.translucent=NO;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStyleDone target:self action:@selector(menuPressed)];
    // Do any additional setup after loading the view from its nib.
}
-(void)menuPressed{
    CGRect destination = self.navigationController.view.frame;
    
    if (destination.origin.x > 0)
    {
        destination.origin.x = 0;
    }else
    {
        destination.origin.x = 220;
    }
    
    [UIView animateWithDuration:0.25 animations:^
     {
         self.navigationController.view.frame = destination;
         
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark collectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return imgArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    UIView *view = [[UIView alloc]init];
    view.frame = CGRectMake(0, 0, 67.5, 68);
    view.layer.cornerRadius = 7.0;
    view.layer.borderColor = [UIColor whiteColor].CGColor;
    view.layer.borderWidth = 1.0;
    view.backgroundColor =  [UIColor colorWithRed:0.2627 green:0.5804 blue:0.9647 alpha:1.0];
    [cell addSubview:view];
    imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,60,60)];
    imgView.contentMode = UIViewContentModeCenter;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
   // [imgView setImageWithURL:[NSURL URLWithString:@""] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
    imgView.image = [imgArray objectAtIndex:indexPath.row];
    [cell addSubview:imgView];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
  
        
        return CGSizeMake(67.5, 68);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    self.imgViewPopUp.image = [imgArray objectAtIndex:indexPath.row];
    NSLog(@"%@",[lbl1Array objectAtIndex:indexPath.row]);
    
    self.LBLURSA.text = [lbl1Array objectAtIndex:indexPath.row];
    self.lblWas.text = [lbl2Array objectAtIndex:indexPath.row];
    
    if(!self.popup)
        self.popup = [KOPopupView popupView];
    [self.popup.handleView addSubview:self.alertView];
    self.alertView.center = CGPointMake(self.popup.handleView.frame.size.width/2.0,
                                        self.popup.handleView.frame.size.height/2.0);
    [self.popup show];

    
    
}




- (IBAction)btnHidePressed:(id)sender {
    [self.popup hideAnimated:YES];
}
- (IBAction)btnCall:(id)sender {
    
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView  *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }

    
}
- (IBAction)btnEmail:(id)sender {
    
    if (comapny_info.count==0) {
        [self performSelectorOnMainThread:@selector(DataAlert) withObject:nil waitUntilDone:NO];
    }else {
        
        mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"Automated Repair Request"];
        NSArray *toBcc = [NSArray arrayWithObject:@"dealernx@gmail.com"];
        [mailComposer setBccRecipients:toBcc];
        
        
        
        NSArray *toRecipients = [NSArray arrayWithObject:btnEmail];
        [mailComposer setToRecipients:toRecipients];
        
        [mailComposer setMessageBody:email_body isHTML:NO];
        [self presentViewController:mailComposer animated:YES completion:nil];
    }
}


-(void)getSettingInfo{
    //[self showProgressForView:self.navigationController.view WithMessage:@"Loading..."];
    webService_Obj=[WebService shared];
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
    NSString * uid=[[[UIDevice currentDevice]identifierForVendor] UUIDString];
    NSLog(@"uuid = %@",uid);
    
    [dict setValue:@"show_user_info" forKey:@"action"];
    [dict setValue:company_id forKey:@"company_app_id"];
    [dict setValue:uid forKey:@"device_token"];
    NSLog(@"%@",dict);
    
    [webService_Obj POST:dict completion:^(NSDictionary *json, BOOL sucess) {
        // [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
        NSLog(@"%@",json);
        comapny_info = [json valueForKey:@"user_info"];
        
        if (comapny_info == nil || comapny_info ==(id)[NSNull null] || comapny_info.count==0) {
            
            
            //[self performSelectorOnMainThread:@selector(DataAlert) withObject:nil waitUntilDone:NO];
            
        }
        else {
            
            first_name= [[comapny_info objectAtIndex:0]valueForKey:@"first_name"];
            last_name = [[comapny_info objectAtIndex:0]valueForKey:@"last_name"];
            // btnEmail = [[comapny_info objectAtIndex:0]valueForKey:@"email_address"];
            mobile_no = [[comapny_info objectAtIndex:0]valueForKey:@"phone_mobile"];
            home_no = [[comapny_info objectAtIndex:0]valueForKey:@"phone_home"];
            busniess_no = [[comapny_info objectAtIndex:0]valueForKey:@"phone_bussiness"];
            
            vin_no = [[comapny_info objectAtIndex:0]valueForKey:@"vehicle_info"];
            
            service_option = [[comapny_info objectAtIndex:0]valueForKey:@"service_option"];
            
            repair_option =  [[comapny_info objectAtIndex:0]valueForKey:@"repair"];
            
            email_body =[NSString stringWithFormat:@"VIN:%@\nCustomer Name:%@ %@\nCustomer phone number(Home):%@\nCustomer phone number(Mobile):%@\nCustomer phone number(Business):%@\nDrop off required:%@\nRepair completion text required:%@",vin_no,first_name,last_name,home_no,mobile_no,busniess_no,service_option,repair_option];
            NSLog(@"%@",email_body);
            
            
        }
        
    }];
    
}

#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    //  [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)showProgressForView:(UIView *)view WithMessage:(NSString *)message {
    self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    _hud.labelText = message;
}

- (void)hideProgressForView:(UIView*)view {
    if (_hud) {
        [MBProgressHUD hideHUDForView:view animated:YES];
        self.hud = nil;
    }
}

#pragma mark Api
-(void)instrumentClusterApi
{
    
    NSLog(@"chdjsycdjdjksvdknvdjuvdfv");
    
    [self showProgressForView:self.navigationController.view WithMessage:@"Loading..."];
    webService_Obj=[WebService shared];
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    [dict setValue:@"show_Inst_clstr" forKey:@"action"];
    [dict setValue:company_id forKey:@"company_app_id"];
    
    [webService_Obj POST:dict completion:^(NSDictionary *json, BOOL sucess) {
        [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
        NSLog(@"%@",json);
        NSArray *comapny_info1 = [json valueForKey:@"company_info"];
        
        
        strWebView = [[comapny_info1 objectAtIndex:0] valueForKey:@"comp_instrumental_cluster"];
        
        strWebView = [strWebView stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        
        
        
        phNo  = [[comapny_info1 objectAtIndex:0] valueForKey:@"company_call"];
        
        btnEmail = [[comapny_info1 objectAtIndex:0]valueForKey:@"email"];
        NSLog(@"gdsadg");
        
    }];
}

-(void)DataAlert{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter your information in setting page" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

@end
