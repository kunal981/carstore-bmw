//
//  RequestServiceViewController.m
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "RequestServiceViewController.h"
#import "MBProgressHUD.h"
#import "WebService.h"
#import "UIImageView+WebCache.h"
#import "Constant.h"

@interface RequestServiceViewController (){
    WebService * webService_Obj;
    

}

@end

@implementation RequestServiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self requestApi];
    
    [self getSettingInfo];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [self.imgViewBanner setUserInteractionEnabled:YES];
    [self.imgViewBanner addGestureRecognizer:singleTap];
   //  self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg1.png"]];
    self.btnRoadSide.layer.cornerRadius = 3.0;
    self.btnRoadSide.layer.masksToBounds  = YES;
    self.btnRoadSide.backgroundColor =  [UIColor colorWithRed:0.0000 green:0.4784 blue:1.0000 alpha:1.0];
    
    self.btnCall.layer.cornerRadius = 3.0;
    self.btnCall.layer.masksToBounds  = YES;
    
    self.btnEmail.layer.cornerRadius = 3.0;
    self.btnEmail.layer.masksToBounds  = YES;
    
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.translucent=NO;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStyleDone target:self action:@selector(menuPressed)];
    
     // self.imgViewBanner.image = [ UIImage imageNamed:@"banner-img"];
    // Do any additional setup after loading the view from its nib.
}
-(void)menuPressed{
    CGRect destination = self.navigationController.view.frame;
    
    if (destination.origin.x > 0)
    {
        destination.origin.x = 0;
    }else
    {
        destination.origin.x = 220;
    }
    
    [UIView animateWithDuration:0.25 animations:^
     {
         self.navigationController.view.frame = destination;
         
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
    //  [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark -UITapGesture Method

-(void)tapDetected{
    
    NSString *phNo = @"+6046593250";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView  *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (IBAction)btnCall:(id)sender {
    
    //NSString *phNo = @"+6046593250";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",btnCallNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView  *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (IBAction)btnEmail:(id)sender {
    if (comapny_info.count==0) {
        [self performSelectorOnMainThread:@selector(DataAlert) withObject:nil waitUntilDone:NO];
    }else {
        
        mailComposer = [[MFMailComposeViewController alloc]init];
        mailComposer.mailComposeDelegate = self;
        [mailComposer setSubject:@"Automated Repair Request"];
        NSArray *toBcc = [NSArray arrayWithObject:@"dealernx@gmail.com"];
        [mailComposer setBccRecipients:toBcc];
        
        
        
        NSArray *toRecipients = [NSArray arrayWithObject:btnEmail];
        [mailComposer setToRecipients:toRecipients];
        
        [mailComposer setMessageBody:email_body isHTML:NO];
        [self presentViewController:mailComposer animated:YES completion:nil];
    }


}
- (IBAction)btnRoadSide:(id)sender {
   // NSString *phNo = @"+18002678269";
    NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",btnRoadSideNumber]];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
        [[UIApplication sharedApplication] openURL:phoneUrl];
    }
    else
    {
        UIAlertView  *calert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Call facility is not available!!!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [calert show];
    }
}

- (void)showProgressForView:(UIView *)view WithMessage:(NSString *)message {
    self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    _hud.labelText = message;
}

- (void)hideProgressForView:(UIView*)view {
    if (_hud) {
        [MBProgressHUD hideHUDForView:view animated:YES];
        self.hud = nil;
    }
}

#pragma mark  API
-(void)requestApi
{
    [self showProgressForView:self.navigationController.view WithMessage:@"Loading..."];
    webService_Obj=[WebService shared];
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    [dict setValue:@"show_request_ser" forKey:@"action"];
    [dict setValue:company_id forKey:@"company_app_id"];
    
    [webService_Obj POST:dict completion:^(NSDictionary *json, BOOL sucess) {
        [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
        NSLog(@"%@",json);
       
        
        NSArray *comapny_info1 = [json valueForKey:@"company_info"];
        
        NSLog(@"%@",[[comapny_info1 objectAtIndex:0]valueForKey:@"homepage_banner"]);
        
        
        [self.imgViewBanner sd_setImageWithURL:[NSURL URLWithString:[[comapny_info1 objectAtIndex:0]valueForKey:@"homepage_banner"]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
        
       strContent = [[comapny_info1  objectAtIndex:0] valueForKey:@"request_service_content"];
        NSString *strEmail =[[comapny_info1 objectAtIndex:0]valueForKey:@"email"];
        NSString *strCall = [[comapny_info1 objectAtIndex:0]valueForKey:@"company_call"];
        NSString *strRoadSide = [[comapny_info1 objectAtIndex:0]valueForKey:@"_roadside_assitance_culst"];
        
        btnEmail = [NSString stringWithFormat:@"%@",strEmail];
        btnCallNumber = [NSString stringWithFormat:@"%@",strCall];
       
        btnRoadSideNumber = [NSString stringWithFormat:@"%@",strRoadSide];
        
        self.lblContent.text = [NSString stringWithFormat:@"%@",strContent];
        
    }];
}

-(void)getSettingInfo{
    //[self showProgressForView:self.navigationController.view WithMessage:@"Loading..."];
    webService_Obj=[WebService shared];
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
    NSString * uid=[[[UIDevice currentDevice]identifierForVendor] UUIDString];
    NSLog(@"uuid = %@",uid);
    
    [dict setValue:@"show_user_info" forKey:@"action"];
    [dict setValue:company_id forKey:@"company_app_id"];
    [dict setValue:uid forKey:@"device_token"];
    NSLog(@"%@",dict);
    
    [webService_Obj POST:dict completion:^(NSDictionary *json, BOOL sucess) {
        // [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
        NSLog(@"%@",json);
        comapny_info = [json valueForKey:@"user_info"];
        
        if (comapny_info == nil || comapny_info ==(id)[NSNull null] || comapny_info.count==0) {
            
            
            //[self performSelectorOnMainThread:@selector(DataAlert) withObject:nil waitUntilDone:NO];
            
        }
        else {
            
            first_name= [[comapny_info objectAtIndex:0]valueForKey:@"first_name"];
            last_name = [[comapny_info objectAtIndex:0]valueForKey:@"last_name"];
           // btnEmail = [[comapny_info objectAtIndex:0]valueForKey:@"email_address"];
            mobile_no = [[comapny_info objectAtIndex:0]valueForKey:@"phone_mobile"];
            home_no = [[comapny_info objectAtIndex:0]valueForKey:@"phone_home"];
            busniess_no = [[comapny_info objectAtIndex:0]valueForKey:@"phone_bussiness"];
            
            vin_no = [[comapny_info objectAtIndex:0]valueForKey:@"vehicle_info"];
            
            service_option = [[comapny_info objectAtIndex:0]valueForKey:@"service_option"];
            
            repair_option =  [[comapny_info objectAtIndex:0]valueForKey:@"repair"];
            
            email_body =[NSString stringWithFormat:@"VIN:%@\nCustomer Name:%@ %@\nCustomer phone number(Home):%@\nCustomer phone number(Mobile):%@\nCustomer phone number(Business):%@\nDrop off required:%@\nRepair completion text required:%@",vin_no,first_name,last_name,home_no,mobile_no,busniess_no,service_option,repair_option];
            NSLog(@"%@",email_body);
            
            
        }
        
    }];
    
}

-(void)DataAlert{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please enter your information in setting page" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
@end
