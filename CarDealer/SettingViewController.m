//
//  SettingViewController.m
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "SettingViewController.h"
#import "ZBarSDK.h"
#import <CoreLocation/CoreLocation.h>
#import "WebService.h"
#import "MBProgressHUD.h"
#import "Constant.h"

@interface SettingViewController (){
   WebService * webService_Obj;
}

@end

@implementation SettingViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    geoFenceStatus =@"no";
    serviceStatus = @"no";
    repairStatus = @"no";
    
    [self settingInfoApi];
    
    locationManger = [[CLLocationManager alloc] init];
    locationManger.distanceFilter = kCLDistanceFilterNone;
    locationManger.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locationManger.delegate=self;
    int value=[[[UIDevice currentDevice] systemVersion]intValue];
    if (value>=8)
    {

    [locationManger requestWhenInUseAuthorization];
    [locationManger requestAlwaysAuthorization];
        
  }
   [locationManger startUpdatingLocation];
    
    // self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg1.png"]];
    self.navigationController.navigationBar.translucent=NO;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStyleDone target:self action:@selector(menuPressed)];
    
   

    self.btnSave.layer.cornerRadius = 3.0;
    
    self.scrollView.contentSize = CGSizeMake(340, 900);
    
   
    // Do any additional setup after loading the view from its nib.
}



-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    
    latitude = locationManger.location.coordinate.latitude;
    longitude = locationManger.location.coordinate.longitude;
    
    NSLog(@"%f,%f",latitude,longitude);

    
}

-(void)menuPressed{
    CGRect destination = self.navigationController.view.frame;
    
    if (destination.origin.x > 0)
    {
        destination.origin.x = 0;
    }else
    {
        destination.origin.x = 220;
    }
    
    [UIView animateWithDuration:0.25 animations:^
     {
         self.navigationController.view.frame = destination;
         
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnScan:(id)sender {
    
    ZBarReaderViewController *codeReader = [ZBarReaderViewController new];
    codeReader.readerDelegate=self;
    codeReader.supportedOrientationsMask = ZBarOrientationMaskAll;
    ZBarImageScanner *scanner = codeReader.scanner;
    [scanner setSymbology: ZBAR_I25 config: ZBAR_CFG_ENABLE to: 0];
    [self presentViewController:codeReader animated:YES completion:nil];
}

- (void)imagePickerController: (UIImagePickerController*) reader didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    
    
    id<NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    NSLog(@"%@",info);
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // just grab the first barcode
        break;
    NSLog(@"data %@",info);
    // showing the result on textview
    NSString *QRcodeString=[[NSString alloc]init];
    QRcodeString=symbol.data;
    NSLog(@"%@",QRcodeString);
    
    self.txtFieldVIN.text = QRcodeString;
    
    
    [reader dismissViewControllerAnimated:YES completion:nil];
    
}



-(void)geoFance{
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(geoLatitude ,geoLongitude);
    CLCircularRegion *region = [[CLCircularRegion alloc]initWithCenter:coord radius:geoRadius identifier:@"SF"];
    locationManger.delegate = self;
    
    ;
    if ([CLLocationManager isMonitoringAvailableForClass:[CLCircularRegion class]]) {
        [locationManger startMonitoringForRegion:region]; 
    }
   
    }

- (void)locationManager:(CLLocationManager *)manager
      didDetermineState:(CLRegionState)state
              forRegion:(CLRegion *)region{
    
    if(state == CLRegionStateInside)
    {
        NSLog(@"##Entered Region - %@", region.identifier);
    }
    else if(state == CLRegionStateOutside)
    {
        NSLog(@"##Exited Region - %@", region.identifier);
    }
    else{
        NSLog(@"##Unknown state  Region - %@", region.identifier);
    }

}



- (void)startMonitoringForRegion:(CLRegion *)region{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Start montoring" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)requestStateForRegion:(CLRegion *)region{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"already in region" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)locationManager:(CLLocationManager *)manager
didStartMonitoringForRegion:(CLRegion *)region{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Start montoring" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    
}

- (void)stopMonitoringForRegion:(CLRegion *)region{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Stop montoring" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)locationManager:(CLLocationManager *)manager
         didEnterRegion:(CLRegion *)region{
    
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    // Notification details
    localNotif.alertBody = @"Enter into region";
    // Set the action button
    localNotif.alertAction = @"View";
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    
    // Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
   
   UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Enter in region" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
   [alert show];
    
}

- (void)locationManager:(CLLocationManager *)manager
monitoringDidFailForRegion:(CLRegion *)region
              withError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Error" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    // Notification details
    localNotif.alertBody = @"Exit from region";
    // Set the action button
    localNotif.alertAction = @"View";
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    localNotif.applicationIconBadgeNumber = 1;
    
    
    // Schedule the notification
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];

    
   UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Exit from region" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

- (BOOL)containsCoordinate:(CLLocationCoordinate2D)coordinate{
    NSLog(@"%f %f",coordinate.latitude,coordinate.longitude);
    if (YES) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Already in the region" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
   

    
    return YES;
}


- (IBAction)btnServiceOption1:(id)sender {
    
    
}

- (IBAction)btnServiceOption2:(id)sender {
    
   
}

- (IBAction)btnGeoFence:(id)sender {
    
    

}





- (IBAction)btnSave:(id)sender {
    
    [self upateSettingInfoApi];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)showProgressForView:(UIView *)view WithMessage:(NSString *)message {
    self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    _hud.labelText = message;
}

- (void)hideProgressForView:(UIView*)view {
    if (_hud) {
        [MBProgressHUD hideHUDForView:view animated:YES];
        self.hud = nil;
    }
}




#pragma mark  API
-(void)settingInfoApi
{
    [self showProgressForView:self.navigationController.view WithMessage:@"Loading..."];
    webService_Obj=[WebService shared];
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
    NSString * uid=[[[UIDevice currentDevice]identifierForVendor] UUIDString];
    NSLog(@"uuid = %@",uid);
    
    [dict setValue:@"show_user_info" forKey:@"action"];
    [dict setValue:company_id forKey:@"company_app_id"];
    [dict setValue:uid forKey:@"device_token"];
    NSLog(@"%@",dict);
    
    [webService_Obj POST:dict completion:^(NSDictionary *json, BOOL sucess) {
        [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
        NSLog(@"%@",json);
        NSArray *comapny_info = [json valueForKey:@"user_info"];
        
        if (comapny_info == nil || comapny_info ==(id)[NSNull null] || comapny_info.count==0) {
            
            NSString *strMsg = [json valueForKey:@"msg"];
            [self performSelectorOnMainThread:@selector(noDataAlert:) withObject:strMsg waitUntilDone:NO];
            
        }else {
        
        self.txtFirstName.text = [[comapny_info objectAtIndex:0]valueForKey:@"first_name"];
        self.txtLastName.text = [[comapny_info objectAtIndex:0]valueForKey:@"last_name"];
        self.txtEmailAddress.text = [[comapny_info objectAtIndex:0]valueForKey:@"email_address"];
        self.txtPhoneNo.text = [[comapny_info objectAtIndex:0]valueForKey:@"phone_mobile"];
        self.txtMobile.text = [[comapny_info objectAtIndex:0]valueForKey:@"phone_home"];
        self.txtBusiness.text = [[comapny_info objectAtIndex:0]valueForKey:@"phone_bussiness"];
        
        self.txtFieldVIN.text = [[comapny_info objectAtIndex:0]valueForKey:@"vehicle_info"];
            
            NSString *service = [[comapny_info objectAtIndex:0]valueForKey:@"service_option"];
            
            NSString *repair =  [[comapny_info objectAtIndex:0]valueForKey:@"repair"];
            
            NSString *geofence = [[comapny_info objectAtIndex:0]valueForKey:@"geo_fences"];
            
            if ([service isEqualToString:@"yes"]) {
                [self.switchServiceOption1 setOn:YES];
                
            }else {
                [self.switchServiceOption1 setOn:NO];
            }
            if ([repair isEqualToString:@"yes"]) {
                
                [self.switchServiceOption2 setOn:YES];
                
            }else{
                [self.switchServiceOption2 setOn:NO];
            }
            
            if ([geofence isEqualToString:@"yes"]) {
                [self.switchGeoFence setOn:YES];
            }else {
               [self.switchGeoFence setOn:NO];
            }
        }
        
    }];
}


-(void)noDataAlert:(NSString*)msg{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)upateSettingInfoApi
{
    
    if (self.txtFirstName.text.length==0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter first name." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtLastName.text.length==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter last name." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtPhoneNo.text.length ==0||self.txtMobile.text.length==0||self.txtBusiness.text.length==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter phone no." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (self.txtFieldVIN.text.length ==0){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Please enter vin no." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else {
    
    [self showProgressForView:self.navigationController.view WithMessage:@"Loading..."];
    webService_Obj=[WebService shared];
        
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    NSString * uid=[[[UIDevice currentDevice]identifierForVendor] UUIDString];
   
    [dict setValue:@"add_user_info" forKey:@"action"];
    [dict setValue:company_id forKey:@"company_app_id"];
    [dict setValue:uid forKey:@"device_token"];
     [dict setValue:self.txtFirstName.text forKey:@"first_name"];
    [dict setValue:self.txtLastName.text forKey:@"last_name"];
    [dict setValue:self.txtEmailAddress.text
            forKey:@"email_address"];
    [dict setValue:self.txtPhoneNo.text forKey:@"phone_home"];
    [dict setValue:self.txtMobile.text forKey:@"phone_mobile"];
     [dict setValue:self.txtBusiness.text forKey:@"phone_bussiness"];
        
    [dict setValue:self.txtFieldVIN.text forKey:@"vehicle_info"];
    [dict setValue:serviceStatus forKey:@"service_option"];
      [dict setValue:repairStatus forKey:@"repair"];
    [dict setValue:geoFenceStatus forKey:@"geo_fences"];
        NSLog(@"%@,%@,%@",serviceStatus,repairStatus,geoFenceStatus);
    NSLog(@"%@",dict);
    
    
    
    [webService_Obj POST:dict completion:^(NSDictionary *json, BOOL sucess) {
        
        [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
        NSLog(@"%@",json);
      }];
    }
}


- (IBAction)switchGeoFence:(id)sender {
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(geoLatitude,geoLongitude);
    CLCircularRegion *region = [[CLCircularRegion alloc]initWithCenter:coord radius:geoRadius identifier:@"SF"];
    locationManger.delegate = self;

    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        NSLog(@"its on!");
        geoFenceStatus = @"yes";
         [self geoFance];
    } else {
        geoFenceStatus = @"no";
        [locationManger stopMonitoringForRegion:region];
    }
   
}

- (IBAction)switchServiceOption1:(id)sender {
    
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        
        serviceStatus  = @"yes";
        NSLog(@"its on!");
    } else {
        NSLog(@"its off!");
        
        serviceStatus = @"no";
    }
}

- (IBAction)switchServiceoption2:(id)sender {
    
    UISwitch *mySwitch = (UISwitch *)sender;
    if ([mySwitch isOn]) {
        NSLog(@"its on!");
        
        repairStatus = @"yes";
    } else {
        NSLog(@"its off!");
        repairStatus = @"no";
    }
}
@end
