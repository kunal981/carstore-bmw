//
//  Constant.m
//  CarDealer
//
//  Created by brst on 27/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "Constant.h"

@implementation Constant

 NSString* const company_id= @"BMW_716";
 NSString* const mapAddress = @"1740 W 5th Ave.Vancouver B.C. V6J 1P2";
 NSString* const navTitle = @"TheBMWStore\nVancouver";


float  geoLatitude = 51.04882;
float  geoLongitude = -114.06825;
int geoRadius = 100;
@end
