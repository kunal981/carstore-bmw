//
//  SpecialViewController.h
//  CarDealer
//
//  Created by brst on 20/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end
