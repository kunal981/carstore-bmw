//
//  MapViewController.m
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController (){
    CLLocationCoordinate2D _coordinate;
    
    NSMutableArray *array;
    int map;
    
   

}

@end

@implementation MapViewController
@synthesize mapView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    map =1;
    self.navigationController.navigationBar.translucent=NO;
     self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"menu-icon"] style:UIBarButtonItemStyleDone target:self action:@selector(menuPressed)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Get Directions" style:UIBarButtonItemStyleDone target:self action:@selector(GetDirectionPressed)];
    
    mapView.delegate=self;
    
   
    
    self.routeDataDict = [[NSMutableDictionary alloc] init];
    
    self.mapView.showsUserLocation = YES;
    
    [self setMapRegionWithCoordinate];
    
    //Initialize annotation
   // float lat =[[[NSUserDefaults standardUserDefaults]valueForKey:@"destLat"]floatValue];
    //float longit = [[[NSUserDefaults standardUserDefaults]valueForKey:@"destLng"]floatValue];

    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake(49.2671202, -123.1433779);
    
    annotation.title = @"1740 W 5th Ave.Vancouver B.C. V6J 1P2";
    [mapView addAnnotation:annotation];
    
    
    //Add them to array
    
}



-(void)menuPressed{
    CGRect destination = self.navigationController.view.frame;
    
    if (destination.origin.x > 0)
    {
        destination.origin.x = 0;
    }else
    {
        destination.origin.x = 220;
    }
    
    [UIView animateWithDuration:0.25 animations:^
     {
         self.navigationController.view.frame = destination;
         
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/








-(void)GetDirectionPressed{
    float lat =[[[NSUserDefaults standardUserDefaults]valueForKey:@"destLat"]floatValue];
    float longit = [[[NSUserDefaults standardUserDefaults]valueForKey:@"destLng"]floatValue];
    
    [self.mapView removeOverlays:self.mapView.overlays];
    
    
    NSString* str=[[NSString alloc]init];
    
    str=[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false&avoid=highways&mode=driving",_coordinate.latitude,_coordinate.longitude,lat,longit];
    
    NSLog(@"requesting for : %@", str);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:str]];
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    if (response==nil || response ==(id)[NSNull null]) {
        NSLog(@"route not created");
        
    }else {
    
    NSError *errors = nil;
    
    self.routeDataDict=[NSJSONSerialization JSONObjectWithData:response options:2 error:&errors];
    
    NSLog(@"%@",[[[[self.routeDataDict objectForKey:@"routes"] lastObject] objectForKey:@"overview_polyline"] objectForKey:@"points"]);
        
        if ([[[[self.routeDataDict objectForKey:@"routes"] lastObject] objectForKey:@"overview_polyline"] objectForKey:@"points"]==nil || [[[[self.routeDataDict objectForKey:@"routes"] lastObject] objectForKey:@"overview_polyline"] objectForKey:@"points"]==(id)[NSNull null]) {
            
             NSLog(@"route not created");
            
        }else {
    
    [self drawRoute:[self decodePolyLine:[[[[[self.routeDataDict objectForKey:@"routes"] lastObject] objectForKey:@"overview_polyline"] objectForKey:@"points"] mutableCopy]]];
            
        }
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    _coordinate.latitude = userLocation.location.coordinate.latitude;
    
    _coordinate.longitude = userLocation.location.coordinate.longitude;
    
    if (map==1) {
        
    
    
    [self setMapRegionWithCoordinate:_coordinate];
        map++;
    }
}

- (void)setMapRegionWithCoordinate

{
    
    CLLocationCoordinate2D coordinate;
    
    MKCoordinateRegion region;
    
    region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.4, 0.6));
    
    MKCoordinateRegion adjustedRegion = [mapView regionThatFits:region];
    
    [mapView setRegion:adjustedRegion animated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    finalArray=[[NSMutableArray alloc]init];
    
    [self.locationManager startUpdatingLocation];
    
    self.locationManager = [[CLLocationManager alloc]init];
    
    self.locationManager.delegate = self;
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    
    int value=[[[UIDevice currentDevice] systemVersion]intValue];
    if (value>=8)
    {
        
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
        
    }
   
    
    
    
    
}





#pragma mark - Route

-(NSMutableArray *)decodePolyLine:(NSMutableString *)encodedString
{
    [encodedString replaceOccurrencesOfString:@"\\\\" withString:@"\\"options:NSLiteralSearch range:NSMakeRange(0, [encodedString length])];
    
    NSInteger len = [encodedString length];
    
    NSInteger index = 0;
    
    array= [[NSMutableArray alloc] init];
    
    NSInteger lat=0;
    
    NSInteger lng=0;
    
    while (index < len)
    {
        NSInteger b;
        
        NSInteger shift = 0;
        
        NSInteger result = 0;
        
        do
        {
            b = [encodedString characterAtIndex:index++] - 63;
            
            result |= (b & 0x1f) << shift;
            
            shift += 5;
            
        }
        while (b >= 0x20);
        
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        
        lat += dlat;
        
        shift = 0;
        
        result = 0;
        
        do
        {
            b = [encodedString characterAtIndex:index++] - 63;
            
            result |= (b & 0x1f) << shift;
            
            shift += 5;
            
        }
        while (b >= 0x20);
        
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        
        lng += dlng;
        
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        
        printf("\n[%f,", [latitude doubleValue]);
        
        printf("%f]", [longitude doubleValue]);
        
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        
        [array addObject:loc];
        
    }
    
    return array;
    
}


- (void) drawRoute:(NSArray *) path
{
    
    NSInteger numberOfSteps = path.count;
    
    CLLocationCoordinate2D coordinates[numberOfSteps];
    
    for (NSInteger index = 0; index < numberOfSteps; index++)
    {
        CLLocation *location = [path objectAtIndex:index];
        
        CLLocationCoordinate2D coordinate = location.coordinate;
        
        coordinates[index] = coordinate;
        
        _coordinate.latitude = coordinate.latitude;
        
        _coordinate.longitude = coordinate.longitude;
        
        [self setMapRegionWithCoordinate:_coordinate];
        
    }
    MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:coordinates count:numberOfSteps];
    
    [self.mapView addOverlay:polyLine];
    
    [self centerMap:[array mutableCopy]];
    
}


- (void)setMapRegionWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    MKCoordinateRegion region;
    
    region = MKCoordinateRegionMake(coordinate, MKCoordinateSpanMake(0.4, 0.6));
    
    MKCoordinateRegion adjustedRegion = [mapView regionThatFits:region];
    
    [mapView setRegion:adjustedRegion animated:YES];
}


- (void)centerMap:(NSMutableArray *)arr
{
    NSLog(@"%@",arr);
    
    MKCoordinateRegion region;
    
    CLLocationDegrees maxLat = -90;
    
    CLLocationDegrees maxLon = -180;
    
    CLLocationDegrees minLat = 90;
    
    CLLocationDegrees minLon = 180;
    
    for(int idx = 0; idx < [arr count]; idx++)
        
    {
        CLLocation* currentLocation1 = [arr objectAtIndex:idx];
        
        if(currentLocation1.coordinate.latitude > maxLat)
            
            maxLat = currentLocation1.coordinate.latitude;
        
        if(currentLocation1.coordinate.latitude < minLat)
            
            minLat = currentLocation1.coordinate.latitude;
        
        if(currentLocation1.coordinate.longitude > maxLon)
            
            maxLon = currentLocation1.coordinate.longitude;
        
        if(currentLocation1.coordinate.longitude < minLon)
            
            minLon = currentLocation1.coordinate.longitude;
    }
    region.center.latitude = (maxLat + minLat) / 2;
    
    region.center.longitude = (maxLon + minLon) / 2;
    
    region.span.latitudeDelta = maxLat - minLat;
    
    region.span.longitudeDelta = maxLon - minLon;
    
    [self.mapView setRegion:region animated:YES ];
    
}


#pragma mark - polyline

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if([overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolylineRenderer *polylineRender = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
        
        polylineRender.strokeColor = [UIColor blueColor];
        
        polylineRender.lineWidth = 2;
        
        return polylineRender;
    }
    return nil;
}


#pragma mark - segmentcontroll


//#pragma mark - pinAnnotation & delegate
//
//- (MKAnnotationView *) mapView: (MKMapView *) mapView viewForAnnotation:(id<MKAnnotation>) annotation
//{
//    if (annotation == mapView.userLocation)
//    {
//        return nil;
//    }
//    else
//    {
//        MKAnnotationView *pin = (MKAnnotationView *) [self.mapView dequeueReusableAnnotationViewWithIdentifier: @"VoteSpotPin"];
//        if (pin == nil)
//        {
//            pin = [[MKAnnotationView alloc] initWithAnnotation: annotation reuseIdentifier: @"TestPin"];
//        }
//        else
//        {
//            pin.annotation = annotation;
//        }
//        
//        [pin setImage:[UIImage imageNamed:@"TestPin.png"]];
//        pin.canShowCallout = YES;
//        pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//        return pin;
//    }
//}
//- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
//
//{
//    if(![view.annotation isKindOfClass:[MKUserLocation class]])
//    {
//        CGSize  calloutSize = CGSizeMake(100.0, 80.0);
//        
//        UIView *calloutView = [[UIView alloc] initWithFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y-calloutSize.height, calloutSize.width, calloutSize.height)];
//        
//        calloutView.backgroundColor = [UIColor whiteColor];
//        
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//        
//        button.frame = CGRectMake(5.0, 5.0, calloutSize.width - 10.0, calloutSize.height - 10.0);
//        
//        [button setTitle:@"OK" forState:UIControlStateNormal];
//        
//        //        [button addTarget:self action:@selector(checkin) forControlEvents:UIControlEventTouchUpInside];
//        
//        //[calloutView addSubview:button];
//        
//        
//       // [view.superview addSubview:calloutView];
//        
//    }
//}




@end
