//
//  HomeMenuViewController.h
//  CarDealer
//
//  Created by brst on 08/01/15.
//  Copyright (c) 2015 brst. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MessageUI/MFMailComposeViewController.h>
@class MBProgressHUD;
@interface HomeMenuViewController : UIViewController<MFMailComposeViewControllerDelegate,CLLocationManagerDelegate,MKMapViewDelegate,MKOverlay>{
    MFMailComposeViewController *mailComposer;
    CLLocationManager *locationManager;
    MKRoute *_currentRoute;
    MKPolyline *_routeOverlay;
    
    float sourceLatitude;
    float sourceLongitude;
    
    float destinationLatitude;
    float destinationLongitude;
    
    NSString *bannerNumber;
    
    NSString *btnCallNumber;
    NSString *btnRoadSideNumber;
    NSString *btn_Email;
    
    NSString *first_name;
    NSString *last_name;
    NSString *vin_no;
    NSString *service_option;
    NSString *repair_option;
    NSString *home_no;
    NSString *mobile_no;
    NSString *busniess_no;
    NSString *email_body;
    NSArray *comapny_info;
}



@property (strong, nonatomic) MBProgressHUD *hud;
@property (strong, nonatomic) IBOutlet UIImageView *imgViewBanner;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UILabel *lblRoadside;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedBtn;
- (IBAction)segmentedBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRoad;
- (IBAction)btnRoadSide:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnCall;
- (IBAction)btnEmail:(id)sender;
- (IBAction)btnCall:(id)sender;

@end
