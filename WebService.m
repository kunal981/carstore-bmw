//
//  WebService.m
//  Singleton
//
//  Created by brst on 11/12/14.
//  Copyright (c) 2014 brst. All rights reserved.
//

#import "WebService.h"
#import "AFHTTPRequestOperationManager.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

#import "Reachability.h"


#define url @"http://108.167.189.25/~dealernx/dealershipportal/?"


@implementation WebService
static WebService *shared = nil;
+ (WebService *) shared{
    @synchronized(self) {
        if (shared == nil){
            
            NSLog(@"dsadasdsafds");
              shared = [[WebService alloc] init];
        
        }
    }
    
    return shared;
}

- (void)showProgressForView:(UIView *)view WithMessage:(NSString *)message {
    self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    _hud.labelText = message;
}

- (void)hideProgressForView:(UIView*)view {
    if (_hud) {
        [MBProgressHUD hideHUDForView:view animated:YES];
        self.hud = nil;
    }
}


-(void)POST:(NSMutableDictionary*)dict completion:(void(^)(NSDictionary *json,BOOL sucess))completion {
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        NSLog(@"There IS NO internet connection");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Network error" message:@"Please check your internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    else
    {
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            NSDictionary *jsonDictionary = (NSDictionary *)responseObject;
            
            NSLog(@"%@",jsonDictionary);
            
            if (completion)
                completion(jsonDictionary, YES);
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"%@",error);
            completion(nil, NO);
        }];
    }

}



@end
