//
//  WebService.h
//  Singleton
//
//  Created by brst on 11/12/14.
//  Copyright (c) 2014 brst. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MBProgressHUD;
@interface WebService : NSObject


+(WebService*)shared;

-(void)POST:(NSMutableDictionary*)dict completion:(void(^)(NSDictionary *json,BOOL sucess))completion;


@property (nonatomic, strong) NSDictionary *loginDict;
@property (nonatomic, strong) MBProgressHUD *hud;

@end
